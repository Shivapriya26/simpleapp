# simpleApp

## Objective

The focus of this assignment is to setup and configuration of a basic CI/CD pipeline for a standalone application. I will sbe showing two methods that worked for me.
1.  Using Webhook
2. Trigger using **.gitlab-ci.yml** file

> NOTE: Only with the use of a public IP will both methods function properly. Use ngrok to convert localhost into public ip

## METHOD-1: Using Webhook

1. This is a simple NodeJs app. The functionality of the app is to print a message. 
2. The Gitlab is integrated with Jenkins to automatically build by each commits.
3. Jenkins is used for CI/CD.
4. Webhook is used to integrate Jenkins and Gitlab

### Integration Process

1. Install required Plugins [ Git, Node]
2. Create acess token in gitlab
    - Create a personal access token to use the token for all Jenkins integrations of that user.
    - Create a project access token to use the token at the project level only. For instance, you can revoke the token in a project without affecting Jenkins integrations in other projects.
    - Create a group access token to use the token for all Jenkins integrations in all projects of that group
3. Configure the Jenkins server
4. Configure the Jenkins project
5. webhook

- [ ] Refer the documentation for further details(https://docs.gitlab.com/ee/integration/jenkins.html)



### IP configuration

Jenkins URL Plays an important role in the integration process. The localhost url were not able to use for the process and it won't work. So I have used ngrok, (https://ngrok.com/download), To convert them into public IP address.

By using them, I could successfully integrate the jenkins and gitlab for automatic build.

## METHOD-2: Using .gitlab-ci.yml Trigger

This method is used to trigger jenkins pipeline by using .gitlab-ci.yml file. You can find the code I used to trigger in my repository.

### Process

1. Configure ip
2. Create a remote-user in jenkins, Dashboard > Manage > Users > add
3. Login into the jenkins by remote-user credentials
4. Generate access token   Dashboard > Manage > Users > remote-user > configure > token
5. Save the access token
6. With this command, you can trigger jenkins pipeline,

`curl -X POST https://b272-115-96-101-252.ngrok-free.app/job/job-name/build --user remote-user:acess token`

`# Add you public ip instead of this`

### Outputs:
![image info](op1.jfif)

```

cd existing_repo
git remote add origin https://gitlab.com/Shivapriya26/simpleapp.git
git branch -M main
git push -uf origin main
```

